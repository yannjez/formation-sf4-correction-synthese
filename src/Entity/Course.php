<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /** @ORM\Column(type="string")  */
    private $nom;
    /** @ORM\Column(type="date")  */
    private $dateDebut;
    /** @ORM\Column(type="date",nullable=true)  */
    private $dateFin;
    /** @ORM\Column(type="string",nullable=true)  */
    private $portDepart;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDateDebut():?\DateTime
    {
        return $this->dateDebut;
    }

    /**
     * @param mixed $dateDebut
     */
    public function setDateDebut($dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return mixed
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param mixed $dateFin
     */
    public function setDateFin($dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @return mixed
     */
    public function getPortDepart()
    {
        return $this->portDepart;
    }

    /**
     * @param mixed $portDepart
     */
    public function setPortDepart($portDepart): void
    {
        $this->portDepart = $portDepart;
    }


    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return "{$this->nom}( {$this->dateDebut->format('d/m/Y')} ) ";
    }
}
