<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParticipationRepository")
 */
class Participation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="integer", nullable=true)  */
    private $position;

    /** @ORM\ManyToOne(targetEntity="Bateau") */
    private $bateau ;

    /** @ORM\ManyToOne(targetEntity="Skipper") */
    private $skipper ;

    /** @ORM\ManyToOne(targetEntity="Course") */
    private $course;

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getBateau()
    {
        return $this->bateau;
    }

    /**
     * @param mixed $bateau
     */
    public function setBateau($bateau): void
    {
        $this->bateau = $bateau;
    }

    /**
     * @return mixed
     */
    public function getSkipper()
    {
        return $this->skipper;
    }

    /**
     * @param mixed $skipper
     */
    public function setSkipper($skipper): void
    {
        $this->skipper = $skipper;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course): void
    {
        $this->course = $course;
    }



    public function getId()
    {
        return $this->id;
    }
}
