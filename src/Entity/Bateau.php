<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BateauRepository")
 */
class Bateau
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string") */
    private $nom;

    /** @ORM\Column(type="float",nullable=true)  */
    private $longueur;

    /** @ORM\ManyToMany(targetEntity="Sponsor") */
    private $sponsors;


    /**
     * @return mixed
     */
    public function getSponsors()
    {
        return $this->sponsors;
    }

    /**
     * @param mixed $sponsors
     */
    public function setSponsors($sponsors): void
    {
        $this->sponsors = $sponsors;
    }


    public function getNom()
    {
        return $this->nom;
    }


    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function getLongueur()
    {
        return $this->longueur;
    }

    public function setLongueur($longueur): void
    {
        $this->longueur = $longueur;
    }


    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
      return $this->nom;
    }
}
