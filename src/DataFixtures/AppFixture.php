<?php

namespace App\DataFixtures;

use App\Entity\Bateau;
use App\Entity\Course;
use App\Entity\Participation;
use App\Entity\Skipper;
use App\Entity\Sponsor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixture extends Fixture
{

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');
        $bateaux =[];
        $skippers =[];
        $courses =[];
        $sponsors =[];

        for($i=0; $i <5;$i++){
            $course = new Course();
            $course->setNom($faker->streetName);
            $course->setPortDepart($faker->city);
            if ($i === 3) {
                $course->setDateDebut($faker->dateTimeInInterval('now', '+1 year'));
            }
            else if ($i===4){
                    $course->setDateDebut($faker->dateTimeInInterval('-3 days ','-1 days'));
            }
            else {
                $course->setDateDebut( $faker->dateTimeInInterval('-3 days ','-6 months'));
                $dateFin  = $course->getDateDebut()->sub(new \DateInterval('P30D'));
                $course->setDateFin($dateFin);
            }
            $manager->persist($course);
            $courses[]= $course;
        }
        // -- evolutions sponsors ----------------------------
        for($i=0; $i <5;$i++){
            $sponsor = new Sponsor();
            $sponsor->setNom($faker->company);
            $manager->persist($sponsor);
            $sponsors[]= $sponsor;
        }
        // -- evolutions sponsors ----------------------------

        for($i=0; $i <5;$i++){
            $bateau = new Bateau();
            $bateau->setNom($faker->firstNameFemale);
            $bateau->setLongueur($faker->randomFloat(2,5,24));

            // -- evolutions sponsors ----------------------------
            shuffle($sponsors);
            $bateau->setSponsors([$sponsors[0],$sponsors[1]]);
            // -- evolutions sponsors ----------------------------

            $manager->persist($bateau);
            $bateaux[]= $bateau;
        }

        for($i=0; $i <5;$i++){
            $skipper = new Skipper();
            $skipper->setNom($faker->name);
            $skipper->setDateNaissance($faker->dateTimeInInterval('-20 years','-50 years'));
             // -- evolutions sécurité ----------------------------
            $skipper->setLogin('skipper'.$i);
            $skipper->setPassword($this->passwordEncoder->encodePassword($skipper,'test'));
            // -- evolutions sécurité ----------------------------
            $manager->persist($skipper);
            $skippers[]= $skipper;
        }


        foreach ($courses as $course ){
            shuffle($bateaux);
            shuffle($skippers);
            for($i=0; $i <4;$i++){
                $participation  = new Participation();
                $participation->setPosition($i==3 ? -1 : $i+1);
                $participation->setBateau($bateaux[$i]);
                $participation->setSkipper($skippers[$i]);
                $participation->setCourse($course);
                $manager->persist($participation);
            }
        }

        $manager->flush();
    }
}
