<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Course::class);
    }

//    /**
//     * @return Course[] Returns an array of Course objects
//     */

    public function getActiveCourses()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.dateFin is null ')
            ->getQuery()
            ->getResult();
    }

    public function getMotCleIds($motCle)
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select(' distinct c.id')
            ->from('\App\Entity\Participation', 'p')
            ->innerJoin('p.course', 'c')
            ->innerJoin('p.skipper', 's')
            ->innerJoin('p.bateau', 'b')
            ->orderBy('c.dateDebut', 'desc')
            ->andWhere(' ( s.nom like :motcle  or b.nom like :motcle or c.nom like :motcle ) ');
        $builder->setParameter('motcle', "%$motCle%");
        return $builder->getQuery()->getResult();
    }


    public function getCoursesByState($state, $search = null)
    {
        $builder = $this->createQueryBuilder('c')->orderBy('c.dateDebut', 'desc');
        switch ($state) {
            case 'past':
                $builder->andWhere('c.dateFin is not  null ');
                break;
            case 'present':
                $builder->andWhere('c.dateFin is null and c.dateDebut <= CURRENT_TIMESTAMP()');
                break;
            case 'futur':
                $builder->andWhere('c.dateFin is null and c.dateDebut > CURRENT_TIMESTAMP()');
                break;
        }

        // évolution recherche avancée -----------------------------------------------
        if (!empty($search)) {
            foreach ($search as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                switch ($key) {
                    case 'dateDebut':
                        $builder->andWhere('c.dateDebut  >=   :dateDebut');
                        $builder->setParameter('dateDebut', $value);
                        break;
                    case 'dateFin':
                        $builder->andWhere('c.dateDebut  <=   :dateFin');
                        $builder->setParameter('dateFin', $value);
                        break;
                    case 'motCle':
                        $ids = $this->getMotCleIds($search['motCle']);
                        if (empty($ids)) {
                            $builder->andWhere(' 0 = 1  ');
                        } else {
                            $in = [];
                            foreach ($ids as $id) {
                                $in[] = $id['id'];
                            }
                            $builder->add('where', $builder->expr()->in('c.id', ':ids'));
                            $builder->setParameter('ids', $in);
                        }

                        break;
                }
            }
        }
        return $builder->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Course
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
