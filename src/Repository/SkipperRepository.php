<?php

namespace App\Repository;

use App\Entity\Skipper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Skipper|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skipper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skipper[]    findAll()
 * @method Skipper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkipperRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Skipper::class);
    }

//    /**
//     * @return Skipper[] Returns an array of Skipper objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Skipper
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
