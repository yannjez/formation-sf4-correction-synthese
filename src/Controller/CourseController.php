<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Participation;
use App\Form\ParticipationType;
use App\Repository\BateauRepository;
use App\Repository\CourseRepository;
use App\Repository\ParticipationRepository;
use App\Repository\SkipperRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CourseController extends Controller
{
    /**
     * @Route("/{state}", name="courses",requirements={"state"="past|present|futur"},defaults={"state"=null})
     */
    public function index(CourseRepository $repo,$state,FormFactoryInterface $formFactory,Request $request)
    {
        $viewData= [];

        // évolution recherche avancée -----------------------------------------------
        $builder = $formFactory->createBuilder(FormType::class,[]);
        $builder->add('dateDebut',DateType::class,['required'=>false]);
        $builder->add('dateFin',DateType::class,['required'=>false]);
        $builder->add('motCle',TextType::class,['required'=>false]);
        $builder->add('submit',SubmitType::class,['label'=>'Rechercher']);
        $form = $builder->getForm();
        $form->handleRequest($request);
        $liste = $repo->getCoursesByState($state,$form->getData());
        $viewData['form']= $form->createView();

        $viewData['courses'] = $liste;
        return $this->render('course/index.html.twig', $viewData);
    }

    /**
     * @Route("/skippers", name="skippers")
     */
    public function skippers(SkipperRepository $repo)
    {
        $viewData= [];
        $liste = $repo->findBy([],['nom'=>'asc']);
        $viewData['skippers'] = $liste;
        return $this->render('course/skippers.html.twig', $viewData);
    }
    /**
     * @Route("/bateaux", name="bateaux")
     */
    public function bateaux(BateauRepository $repo)
    {
        $viewData= [];
        $liste = $repo->findBy([],['nom'=>'asc']);
        $viewData['bateaux'] = $liste;
        return $this->render('course/bateaux.html.twig', $viewData);
    }
    /**
     * @Route("/course/{course}", name="course")
     * @ParamConverter("course", class="App\Entity\Course")
     */
    public function course($course,ParticipationRepository $repo)
    {
        $viewData= [];
        $viewData['course']= $course;
        $participations = $repo->findBy(['course'=>$course],['position'=>'asc']);
        $viewData['participations']= $participations;
        return $this->render('course/course.html.twig', $viewData);
    }
    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request , FormFactoryInterface $formFactory,CourseRepository $repo)
    {
        $viewData= [];

        $newParticiaption = new Participation();
        $builder = $formFactory->createBuilder(ParticipationType::class,$newParticiaption);
        // on supprime le repository par défault
        $builder->remove('course');
        // on ajoute un nouveau type avec le filtre sur la liste
        $builder->add('course',EntityType::class, [
            'class'=>Course::class,
            'choices'=>$repo->getActiveCourses()
        ]);
        $builder->add('submit',SubmitType::class,['label'=>'Enregistrer']);
        $form = $builder->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($newParticiaption);
            $em->flush();
        }
        $viewData['form']=$form->createView();
        return $this->render('course/inscription.html.twig', $viewData);
    }

    /**
     * @Route("/inscription/skipper", name="inscription_skipper")
     */
    public function inscriptionSkipper(Request $request , FormFactoryInterface $formFactory,CourseRepository $repo){
        $newParticiaption = new Participation();
        $builder = $formFactory->createBuilder(ParticipationType::class,$newParticiaption);
        // on supprime le repository par défault
        $builder->remove('course');
        $builder->remove('skipper');
        // on ajoute un nouveau type avec le filtre sur la liste
        $builder->add('course',EntityType::class, [
            'class'=>Course::class,
            'choices'=>$repo->getActiveCourses()
        ]);
        $builder->add('submit',SubmitType::class,['label'=>'Enregistrer']);
        $form = $builder->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $newParticiaption->setSkipper($this->getUser());
            $em->persist($newParticiaption);
            $em->flush();
        }
        $viewData['form']=$form->createView();
        return $this->render('course/inscription.html.twig', $viewData);
    }


    /**
     * @Route("/login", name="site_login")
     */
    public function login()
    {
        return $this->render('course/login.html.twig');
    }


    /**
     * @Route("/compte/loginCheck", name="site_loginCheck")
     */
    public function loginCheck(){
        exit('resolve login');
    }


    /**
     * @Route("/security/logout", name="security_logout")
     */
    public function logout()
    {
        exit('resole logout');
    }

}
