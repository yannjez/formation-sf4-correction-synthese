<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180516170131 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_7D55AF7D12F7FB51');
        $this->addSql('DROP INDEX IDX_7D55AF7DA9706509');
        $this->addSql('CREATE TEMPORARY TABLE __temp__bateau_sponsor AS SELECT bateau_id, sponsor_id FROM bateau_sponsor');
        $this->addSql('DROP TABLE bateau_sponsor');
        $this->addSql('CREATE TABLE bateau_sponsor (bateau_id INTEGER NOT NULL, sponsor_id INTEGER NOT NULL, PRIMARY KEY(bateau_id, sponsor_id), CONSTRAINT FK_7D55AF7DA9706509 FOREIGN KEY (bateau_id) REFERENCES bateau (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_7D55AF7D12F7FB51 FOREIGN KEY (sponsor_id) REFERENCES sponsor (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO bateau_sponsor (bateau_id, sponsor_id) SELECT bateau_id, sponsor_id FROM __temp__bateau_sponsor');
        $this->addSql('DROP TABLE __temp__bateau_sponsor');
        $this->addSql('CREATE INDEX IDX_7D55AF7D12F7FB51 ON bateau_sponsor (sponsor_id)');
        $this->addSql('CREATE INDEX IDX_7D55AF7DA9706509 ON bateau_sponsor (bateau_id)');
        $this->addSql('DROP INDEX IDX_AB55E24FA9706509');
        $this->addSql('DROP INDEX IDX_AB55E24F1C8A3E6');
        $this->addSql('DROP INDEX IDX_AB55E24F591CC992');
        $this->addSql('CREATE TEMPORARY TABLE __temp__participation AS SELECT id, bateau_id, skipper_id, course_id, position FROM participation');
        $this->addSql('DROP TABLE participation');
        $this->addSql('CREATE TABLE participation (id INTEGER NOT NULL, bateau_id INTEGER DEFAULT NULL, skipper_id INTEGER DEFAULT NULL, course_id INTEGER DEFAULT NULL, position INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_AB55E24FA9706509 FOREIGN KEY (bateau_id) REFERENCES bateau (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AB55E24F1C8A3E6 FOREIGN KEY (skipper_id) REFERENCES skipper (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AB55E24F591CC992 FOREIGN KEY (course_id) REFERENCES course (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO participation (id, bateau_id, skipper_id, course_id, position) SELECT id, bateau_id, skipper_id, course_id, position FROM __temp__participation');
        $this->addSql('DROP TABLE __temp__participation');
        $this->addSql('CREATE INDEX IDX_AB55E24FA9706509 ON participation (bateau_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C8A3E6 ON participation (skipper_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F591CC992 ON participation (course_id)');
        $this->addSql('ALTER TABLE skipper ADD COLUMN login VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE skipper ADD COLUMN password VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_7D55AF7DA9706509');
        $this->addSql('DROP INDEX IDX_7D55AF7D12F7FB51');
        $this->addSql('CREATE TEMPORARY TABLE __temp__bateau_sponsor AS SELECT bateau_id, sponsor_id FROM bateau_sponsor');
        $this->addSql('DROP TABLE bateau_sponsor');
        $this->addSql('CREATE TABLE bateau_sponsor (bateau_id INTEGER NOT NULL, sponsor_id INTEGER NOT NULL, PRIMARY KEY(bateau_id, sponsor_id))');
        $this->addSql('INSERT INTO bateau_sponsor (bateau_id, sponsor_id) SELECT bateau_id, sponsor_id FROM __temp__bateau_sponsor');
        $this->addSql('DROP TABLE __temp__bateau_sponsor');
        $this->addSql('CREATE INDEX IDX_7D55AF7DA9706509 ON bateau_sponsor (bateau_id)');
        $this->addSql('CREATE INDEX IDX_7D55AF7D12F7FB51 ON bateau_sponsor (sponsor_id)');
        $this->addSql('DROP INDEX IDX_AB55E24FA9706509');
        $this->addSql('DROP INDEX IDX_AB55E24F1C8A3E6');
        $this->addSql('DROP INDEX IDX_AB55E24F591CC992');
        $this->addSql('CREATE TEMPORARY TABLE __temp__participation AS SELECT id, bateau_id, skipper_id, course_id, position FROM participation');
        $this->addSql('DROP TABLE participation');
        $this->addSql('CREATE TABLE participation (id INTEGER NOT NULL, bateau_id INTEGER DEFAULT NULL, skipper_id INTEGER DEFAULT NULL, course_id INTEGER DEFAULT NULL, position INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO participation (id, bateau_id, skipper_id, course_id, position) SELECT id, bateau_id, skipper_id, course_id, position FROM __temp__participation');
        $this->addSql('DROP TABLE __temp__participation');
        $this->addSql('CREATE INDEX IDX_AB55E24FA9706509 ON participation (bateau_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C8A3E6 ON participation (skipper_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F591CC992 ON participation (course_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__skipper AS SELECT id, nom, date_naissance FROM skipper');
        $this->addSql('DROP TABLE skipper');
        $this->addSql('CREATE TABLE skipper (id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, date_naissance DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO skipper (id, nom, date_naissance) SELECT id, nom, date_naissance FROM __temp__skipper');
        $this->addSql('DROP TABLE __temp__skipper');
    }
}
