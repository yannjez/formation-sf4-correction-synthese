<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180516103913 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE article (id INTEGER NOT NULL, titre VARCHAR(255) NOT NULL, resume CLOB NOT NULL, slug VARCHAR(255) DEFAULT NULL, contenu CLOB NOT NULL, auteur VARCHAR(255) NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE bateau (id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, longueur DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE course (id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, date_debut DATE NOT NULL, date_fin DATE DEFAULT NULL, port_depart VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE participation (id INTEGER NOT NULL, bateau_id INTEGER DEFAULT NULL, skipper_id INTEGER DEFAULT NULL, course_id INTEGER DEFAULT NULL, position INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AB55E24FA9706509 ON participation (bateau_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C8A3E6 ON participation (skipper_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F591CC992 ON participation (course_id)');
        $this->addSql('CREATE TABLE skipper (id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, date_naissance DATE DEFAULT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE bateau');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE participation');
        $this->addSql('DROP TABLE skipper');
    }
}
